import 'package:flutter/cupertino.dart';
import 'package:meta/meta.dart';

import '../core/core.dart';

@immutable
abstract class NavigatorEvent {
  final Function(dynamic) then;

  NavigatorEvent({this.then});
}

class NavigatorActionPop extends NavigatorEvent {
  final dynamic result;

  NavigatorActionPop({this.result});
}

class NavigateProductInsertScreen extends NavigatorEvent {
  final Product product;
  
  NavigateProductInsertScreen(this.product, {then}) : super(then: then);
}

class NavigatorShowDialog extends NavigatorEvent {
  final Widget widget;
  final String tag;

  NavigatorShowDialog({
    this.widget,
    this.tag,
  });
}

class NavigatorDismissDialog extends NavigatorEvent {
  final String tag;

  NavigatorDismissDialog({
    this.tag,
  });
}

class NavigatorLaunchWeb extends NavigatorEvent {
  final String path;

  NavigatorLaunchWeb(this.path);
}
