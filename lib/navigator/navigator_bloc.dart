import 'dart:async';
import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:ds_ecom/home/home.dart';
import 'package:ds_ecom/product/bloc/product_bloc.dart';
import 'package:ds_ecom/product/product_screen.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import './navigator.dart' as navigator;

class NavigatorBloc extends Bloc<navigator.NavigatorEvent, dynamic> {
  final GlobalKey<NavigatorState> navigatorKey;

  NavigatorBloc({this.navigatorKey});

  @override
  dynamic get initialState => 0;

  @override
  Stream<NavigatorState> mapEventToState(
    navigator.NavigatorEvent event,
  ) async* {
    if (event is navigator.NavigatorActionPop) {
      if (event.result != null) {
        navigatorKey.currentState.pop(event.result);
        return;
      }
      navigatorKey.currentState.pop();
    }

    if (event is navigator.NavigateProductInsertScreen) {
      navigatorKey.currentState.push(
        MaterialPageRoute(
          settings: RouteSettings(name: "Product Insertion"),
          builder: (context) => BlocProvider<ProductBloc>(
            create: (context) => ProductBloc(),
            child: ProductScreen(product: event.product,),
          ),
        ),
      );
    }
  }
}
