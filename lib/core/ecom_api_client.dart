import 'dart:convert';
import 'package:logger/logger.dart';
import 'package:http/http.dart' as http;

var logger = Logger();

class EcomNetworkException implements Exception {
  final int statusCode;

  EcomNetworkException(this.statusCode);
}

class EcomApiClient {
  final http.Client httpClient;
  Map<String, String> _headers = {"Content-Type": "application/json"};

  EcomApiClient(this.httpClient) {
    assert(httpClient != null);
  }

  Future<Map<String, dynamic>> createResource(
    String path,
    Map<String, dynamic> data, {
    Map<String, String> queryParams,
  }) async {
    Uri uri = await _createUri(path, queryParams: queryParams);
    final _dataString = jsonEncode(data);
    print(_dataString);
    final response = await httpClient.post(
      uri,
      body: _dataString,
      headers: _headers,
    );

    return _handleResponse(response);
  }

  Future<Map<String, dynamic>> putResource(
    String path,
    Map<String, dynamic> data, {
    Map<String, String> queryParams,
  }) async {
    Uri uri = await _createUri(path, queryParams: queryParams);
    final _dataString = jsonEncode(data);
    print(_dataString);
    final response = await httpClient.put(
      uri,
      body: _dataString,
      headers: _headers,
    );

    return _handleResponse(response);
  }

  Future<Map<String, dynamic>> updateResource(
    String path,
    Map<String, dynamic> data, {
    Map<String, String> queryParams,
  }) async {
    Uri uri = await _createUri(path, queryParams: queryParams);
    final _dataString = jsonEncode(data);
    final response = await httpClient.patch(
      uri,
      body: _dataString,
      headers: _headers,
    );

    return _handleResponse(response);
  }

  Future<Map<String, dynamic>> deleteResource(String path) async {
    Uri uri = await _createUri(path);
    final response = await httpClient.delete(
      uri,
      headers: _headers,
    );

    return _handleResponse(response);
  }

  Future<dynamic> fetch(
    String path, {
    Map<String, String> queryParams,
  }) async {
    Uri uri = await _createUri(path, queryParams: queryParams);

    print(uri.toString());
    final response = await httpClient.get(
      uri,
      headers: _headers,
    );

    return _handleResponse(response);
  }

  Future<dynamic> fetchWithUrl(
    String url, {
    Map<String, String> queryParams,
  }) async {
    Uri uri = Uri.parse(url);

    print(uri.toString());
    final response = await httpClient.get(
      uri,
      headers: _headers,
    );

    return _handleResponse(response);
  }

  dynamic _handleResponse(http.Response response) {
    if (response.statusCode < 200 || response.statusCode > 204) {
      logger.e("HTTPS", response.statusCode);
      throw EcomNetworkException(response.statusCode);
    }
    try {
      if (response.body.isNotEmpty) {
        return jsonDecode(response.body);
      }
    } catch (e, s) {
      logger.e("HTTPS", e, s);
    }
    return Map();
  }

  Future<Uri> _createUri(
    String path, {
    Map<String, String> queryParams,
    int pageSize,
    int pageNumber,
  }) async {
    Map<String, String> _queryParams = Map();
    if (queryParams != null) {
      _queryParams.addAll(queryParams);
    }

    final uri = Uri.http("ds-ecom.azurewebsites.net", path, _queryParams);
    return uri;
  }
}
