import 'core.dart';

mixin Parser {
  List<Product> parseProducts(dynamic data) {
    List<Product> list = new List();
    (data as List).forEach((json) {
      list.add(Product.fromAttributes(json));
    });
    return list;
  }

  List<ProductDefinition> parseProductDefinitions(dynamic data) {
    List<ProductDefinition> list = new List();
    (data as List).forEach((json) {
      list.add(ProductDefinition.fromAttributes(json));
    });
    return list;
  }
}
