import '../core.dart';
import '../parser.dart';

class DataRepository with Parser {
  final EcomApiClient apiClient;

  DataRepository(this.apiClient);

  Future<List<Product>> fetchProducts() async {
    final response = await apiClient.fetch("/products");
    return parseProducts(response);
  }

  Future<List<ProductDefinition>> fetchProductDefinition(
      Product product) async {
    final response = await apiClient.fetchWithUrl(product.definitionUrl);
    return parseProductDefinitions(response);
  }
}
