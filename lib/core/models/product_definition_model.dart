class ProductDefinition {
  static const TAG = "productDefinition";
  String caption;
  String type;
  bool mandatory;
  String defaultValue;
  String validationMessage;

  ProductDefinition({
    this.caption,
    this.type,
    this.mandatory,
    this.defaultValue,
    this.validationMessage,
  });

  ProductDefinition.fromAttributes(Map<String, dynamic> json) {
    if (json == null) return;
    caption = json['caption'];
    type = json['type'];
    mandatory = json['mandatory'];
    defaultValue = json['defaultValue'];
    validationMessage = json['validationMessage'];
  }

  Map<String, dynamic> toAttributes() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['caption'] = this.caption;
    data['type'] = this.type;
    data['mandatory'] = this.mandatory;
    data['defaultValue'] = this.defaultValue;
    data['validationMessage'] = this.validationMessage;
    return data;
  }
}
