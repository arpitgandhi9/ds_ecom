class Product {
  static const TAG = "product";
  String name;
  String definitionUrl;

  Product({
    this.name,
    this.definitionUrl,
  });

  Product.fromAttributes(Map<String, dynamic> json) {
    if (json == null) return;
    name = json['name'];
    definitionUrl = json['definitionUrl'];
  }

  Map<String, dynamic> toAttributes() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['definitionUrl'] = this.definitionUrl;
    return data;
  }
}
