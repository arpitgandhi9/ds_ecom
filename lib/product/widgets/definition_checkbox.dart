import 'package:flutter/material.dart';

import '../../core/core.dart';

class DefinitionCheckbox extends StatefulWidget {
  final ProductDefinition definition;

  const DefinitionCheckbox(this.definition, {Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _DefinitionCheckbox();
  }
}

class _DefinitionCheckbox extends State<DefinitionCheckbox> {
  bool isChecked;
  @override
  void initState() {
    isChecked = widget.definition.defaultValue != "False";
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return CheckboxListTile(
      title: Text(widget.definition.caption),
      value: isChecked,
      onChanged: (s) => setState(() {
        isChecked = s;
      }),
    );
  }
}
