import 'package:flutter/material.dart';

import '../../core/core.dart';

class DefinitionTextField extends StatefulWidget {
  final ProductDefinition definition;
  final TextInputType inputType;

  const DefinitionTextField(
    this.definition, {
    Key key,
    this.inputType,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _DefinitionTextField();
  }
}

class _DefinitionTextField extends State<DefinitionTextField> {
  String text;

  @override
  void initState() {
    text = widget.definition.defaultValue ?? "";
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      decoration: InputDecoration(
        hintText: widget.definition.caption,
        hasFloatingPlaceholder: true,
      ),
      initialValue: text,
      validator: (value) {
        if (value.isEmpty && (widget.definition.mandatory ?? false)) {
          return widget.definition.validationMessage;
        }
        return null;
      },
    );
  }
}
