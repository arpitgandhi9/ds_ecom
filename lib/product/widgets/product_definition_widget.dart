import 'package:flutter/material.dart';

import '../../core/core.dart';
import '../product.dart';

class ProductDefinitionItem extends StatelessWidget {
  final ProductDefinition definition;

  const ProductDefinitionItem({Key key, this.definition}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(8),
      child: _renderDefinition(),
    );
  }

  _renderDefinition() {
    switch (definition.type) {
      case "bool":
        return DefinitionCheckbox(definition);
      case "int":
        return DefinitionTextField(
          definition,
          inputType: TextInputType.number,
        );
      case "text":
        return DefinitionTextField(
          definition,
          inputType: TextInputType.text,
        );
      default:
    }
    return Text(definition.caption);
  }
}
