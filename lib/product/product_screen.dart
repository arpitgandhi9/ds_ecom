import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../core/core.dart';
import 'product.dart';

class ProductScreen extends StatefulWidget {
  final Product product;

  const ProductScreen({Key key, this.product}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _ProductScreen();
  }
}

class _ProductScreen extends State<ProductScreen> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.product.name),
      ),
      body: _buildBody(context),
    );
  }

  _buildBody(BuildContext context) {
    return BlocBuilder<ProductBloc, ProductState>(
      builder: (context, state) {
        if (state is ProductInitial) {
          BlocProvider.of<ProductBloc>(context).add(
            FetchProductDefinition(context, widget.product),
          );
        }
        if (state is LoadingProduct) {
          return Center(
            child: CupertinoActivityIndicator(),
          );
        }
        if (state is ErrorProduct) {
          return Center(
            child: Text("Oops"),
          );
        }

        final definitions = BlocProvider.of<ProductBloc>(context).definitions;
        return SingleChildScrollView(
          child: Form(
            key: _formKey,
            child: Column(
              children: List.generate(
                definitions.length,
                (index) {
                  return ProductDefinitionItem(
                    definition: definitions[index],
                  );
                },
              ),
            ),
          ),
        );
      },
    );
  }
}
