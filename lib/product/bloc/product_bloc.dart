import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:ds_ecom/core/core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';

part 'product_event.dart';
part 'product_state.dart';

class ProductBloc extends Bloc<ProductEvent, ProductState> {
  List<ProductDefinition> definitions = new List();

  @override
  Stream<ProductState> mapEventToState(
    ProductEvent event,
  ) async* {
    if (event is FetchProductDefinition) {
      yield LoadingProduct();
      try {
        definitions = await RepositoryProvider.of<DataRepository>(event.context)
            .fetchProductDefinition(event.product);
        yield LoadedProduct();
      } catch (e, s) {
        yield ErrorProduct();
        logger.e("Product fetch", e, s);
      }
    }
  }

  @override
  ProductState get initialState => ProductInitial();
}
