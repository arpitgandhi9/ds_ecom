part of 'product_bloc.dart';

@immutable
abstract class ProductState {}

class ProductInitial extends ProductState {}

class LoadingProduct extends ProductState {}

class ErrorProduct extends ProductState {}

class LoadedProduct extends ProductState {}
