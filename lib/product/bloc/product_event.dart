part of 'product_bloc.dart';

@immutable
abstract class ProductEvent {}

class FetchProductDefinition extends ProductEvent {
  final BuildContext context;
  final Product product;

  FetchProductDefinition(this.context, this.product);
}
