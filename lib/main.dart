import 'package:ds_ecom/core/core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:http/http.dart' as http;

import 'home/home.dart';
import 'navigator/navigator_bloc.dart';

final ecomApiClient = EcomApiClient(http.Client());

void main() => runApp(
      MultiRepositoryProvider(
        providers: [
          RepositoryProvider<DataRepository>(
            create: (_) {
              return DataRepository(ecomApiClient);
            },
          ),
        ],
        child: DSEcom(),
      ),
    );

class DSEcom extends StatelessWidget {
  final GlobalKey<NavigatorState> _navigatorKey = GlobalKey();
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<NavigatorBloc>(
          create: (context) {
            return NavigatorBloc(navigatorKey: _navigatorKey);
          },
        ),
      ],
      child: MaterialApp(
        navigatorKey: _navigatorKey,
        theme: ThemeData(
          primarySwatch: Colors.blue,
          inputDecorationTheme: InputDecorationTheme(
            hasFloatingPlaceholder: true,
          )
        ),
        home: BlocProvider<HomeBloc>(
          create: (context) => HomeBloc(),
          child: HomeScreen(),
        ),
      ),
    );
  }
}
