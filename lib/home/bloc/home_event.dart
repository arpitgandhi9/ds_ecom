part of 'home_bloc.dart';

@immutable
abstract class HomeEvent {}

class FetchProducts extends HomeEvent {
  final BuildContext context;

  FetchProducts(this.context);
}
