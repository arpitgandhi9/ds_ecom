part of 'home_bloc.dart';

@immutable
abstract class HomeState {}

class HomeInitial extends HomeState {}

class LoadingHome extends HomeState {}

class ErrorHome extends HomeState {}

class LoadedHome extends HomeState {}
