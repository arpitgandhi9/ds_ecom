import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:ds_ecom/navigator/navigator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';

import '../../core/core.dart';

part 'home_event.dart';
part 'home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  List<Product> products = new List();

  @override
  Stream<HomeState> mapEventToState(
    HomeEvent event,
  ) async* {
    if (event is FetchProducts) {
      yield LoadingHome();
      try {
        products = await RepositoryProvider.of<DataRepository>(event.context)
            .fetchProducts();
        yield LoadedHome();
      } catch (e, s) {
        yield ErrorHome();
        logger.e("Home fetch", e, s);
      }
    }
  }

  @override
  HomeState get initialState => HomeInitial();
}
