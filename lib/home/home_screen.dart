import 'package:ds_ecom/home/home.dart';
import 'package:ds_ecom/navigator/navigator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomeScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _HomeScreen();
  }
}

class _HomeScreen extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
      ),
      body: _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext context) {
    return BlocBuilder<HomeBloc, HomeState>(
      builder: (context, state) {
        if (state is HomeInitial) {
          BlocProvider.of<HomeBloc>(context).add(
            FetchProducts(context),
          );
        }
        if (state is LoadingHome) {
          return Center(
            child: CupertinoActivityIndicator(),
          );
        }
        if (state is ErrorHome) {
          return Center(
            child: Text("Oops"),
          );
        }
        final products = BlocProvider.of<HomeBloc>(context).products;
        return ListView.builder(
          itemCount: products.length,
          itemBuilder: (context, index) {
            return ProductItem(
              product: products[index],
              onPressed: () {
                BlocProvider.of<NavigatorBloc>(context).add(
                  NavigateProductInsertScreen(products[index]),
                );
              },
            );
          },
        );
      },
    );
  }
}
