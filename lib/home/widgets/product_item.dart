import 'package:ds_ecom/core/core.dart';
import 'package:flutter/material.dart';

class ProductItem extends StatelessWidget {
  final Product product;
  final Function onPressed;

  const ProductItem({
    Key key,
    this.product,
    this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      onPressed: onPressed,
      child: Text(
        product.name,
        style: TextStyle(
          color: Colors.black87,
        ),
      ),
    );
  }
}
